# Miscellaneous files

This repository contains miscellaneous files for CIGALE.

- `cigale_introduction.ipynb` is a notebook presenting how CIGALE builds the
  SEDs.
